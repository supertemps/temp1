﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Hubs
{
    public class NotificationHub: Hub
    {
        public async Task SendEventMessage(string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", "none", message);
        }
    }
}
