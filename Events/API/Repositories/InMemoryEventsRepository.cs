﻿using API.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace API.Repositories
{
    public class InMemoryEventsRepository : IEventsRepository
    {
        private readonly IDictionary<int, EventModel> _eventsData;
        private readonly ILogger _logger;

        public InMemoryEventsRepository(
            ILogger<InMemoryEventsRepository> logger)
        {
            _logger = logger;
            _eventsData = new Dictionary<int, EventModel>();
        }

        public event EventHandler EventReached;

        public Task<IEnumerable<EventModel>> AllOrderedByStartDate()
        {
            IEnumerable<EventModel> result = _eventsData.Values.OrderBy(e => e.StartTimeUtc).ToArray();

            return Task.FromResult(result);
        }

        public Task<EventModel> Create(EventModel newEvent)
        {
            if (newEvent == default || newEvent.Id == default)
                throw new ArgumentNullException("Invalid event or ID");

            if (_eventsData.ContainsKey(newEvent.Id))
                throw new InvalidOperationException("Id already exists");

            _eventsData.Add(newEvent.Id, newEvent);

            SetTimerForTrigger(newEvent);

            return Task.FromResult(newEvent);
        }

        public Task Delete(int id)
        {
            _eventsData.Remove(id);

            return Task.CompletedTask;
        }

        public Task<EventModel> Retrieve(int id)
        {
            EventModel result;
            if (_eventsData.TryGetValue(id, out result))
                return Task.FromResult(result);

            throw new KeyNotFoundException("ID not found");
        }

        public Task<EventModel> RetrieveNext(DateTime since)
        {
            var result = AllOrderedByStartDate().Result.SingleOrDefault(e => e.StartTimeUtc >= since);

            return Task.FromResult(result);
        }

        public async Task<EventModel> Update(int id, EventModel updatedEvent)
        {
            if (updatedEvent == default || updatedEvent.Id == default)
                throw new ArgumentNullException("Invalid event or ID");

            if (updatedEvent.Id != id)
                throw new ArgumentException("Event Id is diffrent than speified ID");

            if (!_eventsData.ContainsKey(id))
                throw new KeyNotFoundException("ID not found");

            var existingEvent = await Retrieve(id);
            if (existingEvent.StartTimeUtc != updatedEvent.StartTimeUtc)
                throw new InvalidOperationException("Cannot change start date");

            _eventsData[id] = updatedEvent;

            // Didn't have time to handle start date change for notifications

            return updatedEvent;
        }

        protected void SetTimerForTrigger(EventModel ev)
        {
            System.Timers.Timer timer = new Timer();

            double interval = (ev.StartTimeUtc - DateTime.UtcNow).TotalMilliseconds;

            if (interval <= 0)
                return;

            timer.Interval = interval;
            timer.AutoReset = false;
            timer.Elapsed += delegate { OnEventReached(ev.Id, null); };
            timer.Start();

        }

        private void OnEventReached(Object source, System.Timers.ElapsedEventArgs e)
        {
            EventReached.Invoke((int)source, e);
        }
    }
}