﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Repositories
{
    public interface IEventsRepository
    {

        public event EventHandler EventReached;

        /// <summary>
        /// Create event
        /// </summary>
        /// <returns></returns>
        Task<EventModel> Create(EventModel newEvent);

        /// <summary>
        /// Update event
        /// </summary>
        /// <returns></returns>
        Task<EventModel> Update(int id, EventModel updatedEvent);

        /// <summary>
        /// Retrieve event by ID
        /// </summary>
        /// <returns></returns>
        Task<EventModel> Retrieve(int id);

        /// <summary>
        /// Delete event by ID
        /// </summary>
        /// <returns></returns>
        Task Delete(int id);

        /// <summary>
        /// Retrieve all events by start date
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<EventModel>> AllOrderedByStartDate();

        /// <summary>
        /// Retrieve the next event since the specified date, or default if no next one.
        /// </summary>
        /// <returns></returns>
        Task<EventModel> RetrieveNext(DateTime since);
    }
}
