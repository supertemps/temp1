﻿using System;

namespace API.Models
{
    public class EventModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartTimeUtc { get; set; }
        public DateTime EndTimeUtc { get; set; }

        public override string ToString()
        {
            return
                $"Id = {Id}\n" +
                $"Name = {Name}\n" +
                $"StartTimeUtc = {StartTimeUtc}\n" +
                $"EndTimeUtc = {EndTimeUtc}\n";
        }
    }
}
