﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Hubs;
using API.Models;
using API.Repositories;
using Microsoft.AspNetCore.Mvc;


namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly IEventsRepository _repo;
        private readonly NotificationHub _nots;

        public EventsController(
            IEventsRepository repo,
            NotificationHub nots)
        {
            _repo = repo;
            _nots = nots;
        }

        // GET: api/<EventsController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _repo.AllOrderedByStartDate());
        }

        // GET api/<EventsController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                return Ok(await _repo.Retrieve(id));
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            
        }

        // POST api/<EventsController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] EventModel newEvent)
        {
            try
            {
                var result = await _repo.Create(newEvent);

                _repo.EventReached += _repo_EventReached;

                return Ok(result);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return Conflict(e.Message);
            }
        }

        private void _repo_EventReached(object sender, EventArgs e)
        {
            int id = (int)sender;

            try
            {
                var ev = _repo.Retrieve(id).Result;

                _nots.SendEventMessage($"Event starts: {ev.Name}").Wait();
            }
            catch (KeyNotFoundException)
            {
                return;
            }
            

        }

        // PUT api/<EventsController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] EventModel updatedEvent)
        {
            try
            {
                return Ok(await _repo.Update(id, updatedEvent));
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        // DELETE api/<EventsController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _repo.Delete(id);
             
        }
    }
}
